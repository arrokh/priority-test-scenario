﻿using com.arrokh;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.arrokhs.algorithms
{
    public class TestScenarioPriotization
    {
        struct Paths
        {
            public string scenario;
            public string name;
            public string id;
            public List<int> data;
        }

        struct WeightPath
        {
            public List<int> path;
            public int weight;
        }

        struct ComplexityNode
        {
            public int nodeID;
            public int amountNodeContribution;
            public double complexityValueNode;
        }

        struct ComplexityPath
        {
            public List<int> path;
            public double complexityValuePath;
        }

        struct NodeCoverageValue
        {
            public List<int> path;
            public int nodeCoverage;
        }

        struct DecisionNodeCoverageValue
        {
            public List<int> path;
            public int decisionoNode;
        }

        public struct WeightAssignedToEdges
        {
            /*
             * ----------------------------
             * typeOfCoupling |   weight  |
             * ----------------------------
             * no                   0
             * data                 1
             * stamp                2
             * control              3
             * */
            public int index;
            public string typeOfCoupling;
            public int weight;
            public int from;
            public string fromText;
            public int to;
            public string toText;
        }

        struct WeightOfPathsBasedOnCoupling
        {
            public List<int> path;
            public int weightOfEachPath;
        }

        struct TotalWeightOfEachPath
        {
            public List<int> path;
            public double totalWeight;
        }

        struct PrioritizedScenario
        {
            public string scenario;
            public string id;
            public List<int> path;
            public double totalWeight;
        }

        private List<int>[] _adjList;
        private List<Node> _nodes;

        private List<Paths> dataIndependentPaths = new List<Paths>();

        private List<List<int>> _independentPaths = new List<List<int>>();
        private List<int> _amoundOfNodeContribution = new List<int>();
        private List<List<int>> _nodesAffectedByValues = new List<List<int>>();
        private List<double[]> _nodesValueComplexity = new List<double[]>();

        private List<string> _logIndependentPaths = new List<string>();
        private List<string> _logWeightPaths = new List<string>();

        // Setup Variable as name in paper
        private List<WeightPath> dataWeightOfPaths;
        private List<ComplexityNode> dataComplexityValueNodes;
        private List<ComplexityPath> dataComplexityValuePaths;
        private List<NodeCoverageValue> dataNodeCoverageValues;
        private List<DecisionNodeCoverageValue> dataDecisionNodeCoverageValue;
        private List<WeightOfPathsBasedOnCoupling> dataWeightOfPathsBasedOnCoupling;
        private List<TotalWeightOfEachPath> dataTotalWeightOfEachPath;

        public List<WeightAssignedToEdges> DataWeightAssignedToEdges { get; private set; }
        public List<Node> Nodes { get => _nodes; }

        public TestScenarioPriotization(List<Node> nodes, bool autoAddEdge = true)
        {
            _nodes = nodes;
            _nodes.Sort((nodeX, nodeY) => nodeX.IDLabel - nodeY.IDLabel);

            _adjList = new List<int>[_nodes.Count];
            for (int i = 0; i < _nodes.Count; i++)
                _adjList[i] = new List<int>();

            if (autoAddEdge)
            {
                foreach (var nodeBase in nodes)
                    nodeBase.Out.ForEach(nodeTarget => AddEdge(nodeBase.IDLabel - 1, nodeTarget.IDLabel - 1));
            }

            UpdateIndependentPaths();
            UpdateDataIndependentPaths();
            UpdateWeightPaths();
            UpdateDecisionNodeCoverage();
            UpdateWeightAssignedToEdges();
        }

        private void UpdateIndependentPaths()
        {
            bool[] isVisited = new bool[_nodes.Count];

            // Initial pathList and add first element (0);
            List<int> pathList = new List<int> { 0 };

            _logIndependentPaths.Clear();

            IndependentPathsUtils(0, _nodes.Count - 1, isVisited, pathList);

            /*
             * NEW DATA
             * */
            dataNodeCoverageValues = new List<NodeCoverageValue>();
            foreach (var path in _independentPaths)
            {
                dataNodeCoverageValues.Add(new NodeCoverageValue()
                {
                    path = path,
                    nodeCoverage = path.Count
                });
            }

            ////////////////////
        }

        private void IndependentPathsUtils(int firstNode, int lastNode, bool[] isVisited, List<int> localPathList)
        {
            isVisited[firstNode] = true;

            if (firstNode.Equals(lastNode))
            {
                List<int> paths = new List<int>();
                string temp = "";
                for (int i = 0; i < localPathList.Count; i++)
                {
                    paths.Add(localPathList[i] + 1);

                    if (i == localPathList.Count - 1)
                        temp += ((localPathList[i] + 1) + ".");
                    else
                        temp += ((localPathList[i] + 1) + " > ");
                }

                _independentPaths.Add(paths);
                _logIndependentPaths.Add(temp);

            }

            foreach (var item in _adjList[firstNode])
            {
                if (!isVisited[item])
                {
                    localPathList.Add(item);
                    IndependentPathsUtils(item, lastNode, isVisited, localPathList);

                    localPathList.Remove(item);
                }
            }

            isVisited[firstNode] = false;
        }

        private void UpdateDataIndependentPaths()
        {
            dataIndependentPaths = new List<Paths>();

            for (int i = 0; i < _independentPaths.Count; i++)
            {
                dataIndependentPaths.Add(new Paths()
                {
                    data = _independentPaths[i],
                    id = "P" + i,
                    scenario = "Scenario " + (i + 1),
                    name = ""
                });
            }
        }

        private void UpdateWeightPaths()
        {
            _logWeightPaths.Clear();

            /*
             * NEW DATA
             * */
            dataWeightOfPaths = new List<WeightPath>();
            for (int i = 0; i < _independentPaths.Count; i++)
            {
                dataWeightOfPaths.Add(new WeightPath()
                {
                    path = _independentPaths[i],
                    weight = WeightPathsUtils(i)
                });
            }
            //////////////////////
        }

        private void UpdateDecisionNodeCoverage()
        {
            /*
             * NEW DATA
             * */
            dataDecisionNodeCoverageValue = new List<DecisionNodeCoverageValue>();
            var decisionNodes = _nodes.FindAll(node => node.ShapeType == ShapeType.DECISION_NODE);
            List<int> decisionNodeID = new List<int>();
            foreach (var decisionNode in decisionNodes)
                decisionNodeID.Add(decisionNode.IDLabel);

            foreach (var path in _independentPaths)
            {
                int counter = 0;
                foreach (var desNodeID in decisionNodeID)
                {
                    if (path.Contains(desNodeID))
                        counter++;
                }
                dataDecisionNodeCoverageValue.Add(new DecisionNodeCoverageValue()
                {
                    path = path,
                    decisionoNode = counter
                });
            }
        }

        private void UpdateWeightAssignedToEdges()
        {
            /*
             * NEW DATA
             * */
            DataWeightAssignedToEdges = new List<WeightAssignedToEdges>();
            int index = 0;
            foreach (var node in _nodes)
            {
                foreach (var nextNode in node.Out)
                {
                    DataWeightAssignedToEdges.Add(new WeightAssignedToEdges()
                    {
                        index = index++,
                        from = node.IDLabel,
                        to = nextNode.IDLabel,
                        fromText = node.Text,
                        toText = nextNode.Text,
                        weight = 0
                    });
                }
            }
        }

        public void UpdateDataWeightAssignedToEdge(List<WeightAssignedToEdges> dataEdges)
        {
            DataWeightAssignedToEdges.Clear();
            DataWeightAssignedToEdges = dataEdges;

            UpdateWeightOfEachPath();
            UpdateTotalWeightOfEachPath();
        }

        private void UpdateTotalWeightOfEachPath()
        {
            dataTotalWeightOfEachPath = new List<TotalWeightOfEachPath>();

            for (int i = 0; i < _independentPaths.Count; i++)
            {
                dataTotalWeightOfEachPath.Add(new TotalWeightOfEachPath()
                {
                    path = _independentPaths[i],
                    totalWeight =
                        dataWeightOfPaths[i].weight
                        + dataComplexityValueNodes[i].complexityValueNode
                        + dataNodeCoverageValues[i].nodeCoverage
                        + dataDecisionNodeCoverageValue[i].decisionoNode
                        + dataWeightOfPathsBasedOnCoupling[i].weightOfEachPath
                });
            }
        }

        private void UpdateWeightOfEachPath()
        {
            dataWeightOfPathsBasedOnCoupling = new List<WeightOfPathsBasedOnCoupling>();

            foreach (var independentPath in _independentPaths)
            {
                WeightOfPathsBasedOnCoupling data = new WeightOfPathsBasedOnCoupling() { weightOfEachPath = 0 };
                for (int i = 0; i < independentPath.Count - 2; i++)
                {
                    foreach (var edge in DataWeightAssignedToEdges)
                    {
                        if (independentPath[i] == edge.from &&
                            independentPath[i + 1] == edge.to)
                        {
                            data.path = independentPath;
                            data.weightOfEachPath += edge.weight;
                        }
                    }
                }
                dataWeightOfPathsBasedOnCoupling.Add(data);
            }
        }

        private void AddEdge(int nodeBase, int nodeTarget)
        {
            _adjList[nodeBase].Add(nodeTarget);
        }

        public int WeightPathsUtils(int indexSolution)
        {
            if (indexSolution > _independentPaths.Count)
            {
                Debug.Log("Graph::GetWeightPath(int) => Paramter not valid");
                return -1;
            }

            int weightPath = 0;

            foreach (var indexNode in _independentPaths[indexSolution])
            {
                foreach (var node in _nodes)
                {
                    if ((node.IDLabel) == indexNode)
                        weightPath += (node.In.Count * node.Out.Count);
                }
            }

            return weightPath;
        }

        public void UpdateNodeComplexity(int[] nodes)
        {
            _nodesAffectedByValues.Clear();

            var containerEachIndependentPath = new List<List<int>>();
            var affectedNodes = new List<int>();

            foreach (var node in nodes)
            {
                foreach (var path in _independentPaths)
                {
                    for (int i = 0; i < path.Count; i++)
                    {
                        if (path[i] == node)
                        {
                            path
                                .GetRange(i + 1, path.Count - (i + 1))
                                .ForEach(data => { affectedNodes.Add(data); });
                            break;
                        }
                    }
                }
                affectedNodes.Sort((x, y) => x - y);
                _nodesAffectedByValues.Add(affectedNodes.Distinct().ToList());
                affectedNodes = new List<int>();
            }


            foreach (var nodeComplex in _nodesAffectedByValues)
            {
                foreach (var node in nodeComplex)
                    _amoundOfNodeContribution.Add(node);
            }
            _amoundOfNodeContribution.Sort();

            UpdateValueComplexity();
        }

        private void UpdateValueComplexity()
        {
            var nodeAffectedGroup = _amoundOfNodeContribution.GroupBy(value => value);

            foreach (var data in nodeAffectedGroup)
                _nodesValueComplexity.Add(new double[] { data.Key, data.Count(), -1 });

            for (int i = 1; i <= _nodes.Count; i++)
            {
                if (!_nodesValueComplexity.Any(item => item[0] == i))
                    _nodesValueComplexity.Add(new double[] { i, 0, -1 });
            }

            _nodesValueComplexity.Sort((x, y) => (int)x[0] - (int)y[0]);

            foreach (var nodeValue in _nodesValueComplexity)
            {
                List<int> listNodeAffected = new List<int>();
                foreach (var nodesAffected in _nodesAffectedByValues)
                {
                    foreach (var data in nodesAffected)
                    {
                        if (data == (int)nodeValue[0])
                        {
                            listNodeAffected.AddRange(nodesAffected);
                            break;
                        }

                    }
                }
                var totalNodeEffected = listNodeAffected.Distinct().ToList().Count;
                nodeValue[2] = totalNodeEffected == 0 ?
                    totalNodeEffected :
                    Math.Ceiling((nodeValue[1] / totalNodeEffected) * 100) / 100;
            }


            /*
             * NEW DATA
             * */
            dataComplexityValueNodes = new List<ComplexityNode>();
            foreach (var data in _nodesValueComplexity)
            {
                dataComplexityValueNodes.Add(new ComplexityNode()
                {
                    nodeID = (int)data[0],
                    amountNodeContribution = (int)data[1],
                    complexityValueNode = data[2]
                });
            }

            dataComplexityValuePaths = new List<ComplexityPath>();
            foreach (var path in _independentPaths)
            {
                double complexityValuePath = 0;
                foreach (var node in path)
                {
                    var data = dataComplexityValueNodes.Find(x => x.nodeID == node);
                    complexityValuePath += data.complexityValueNode;
                }
                dataComplexityValuePaths.Add(new ComplexityPath()
                {
                    path = path,
                    complexityValuePath = complexityValuePath
                });
            }
        }

        private string PathToString(List<int> path)
        {
            string data = "";
            for (int i = 0; i < path.Count; i++)
            {
                data += path[i];
                if (i != path.Count - 1)
                    data += "-";
            }
            return data;
        }

        public string DataTable1()
        {
            string data = "";
            for (int i = 0; i < dataIndependentPaths.Count; i++)
            {
                data += dataIndependentPaths[i].id + ": " + PathToString(dataIndependentPaths[i].data);
                data += "\n";
            }

            return data;
        }

        public List<string[]> DataTable2()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < _nodes.Count; i++)
            {
                data.Add(new string[4]
                {
                    _nodes[i].IDLabel.ToString(),
                    _nodes[i].In.Count.ToString(),
                    _nodes[i].Out.Count.ToString(),
                    (_nodes[i].In.Count * _nodes[i].Out.Count).ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable3()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataWeightOfPaths.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataIndependentPaths[i].id.ToString(),
                    PathToString(dataWeightOfPaths[i].path),
                    dataWeightOfPaths[i].weight.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable4()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataComplexityValueNodes.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataComplexityValueNodes[i].nodeID.ToString(),
                    dataComplexityValueNodes[i].amountNodeContribution.ToString(),
                    dataComplexityValueNodes[i].complexityValueNode.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable5()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataComplexityValuePaths.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataIndependentPaths[i].id.ToString(),
                    PathToString(dataComplexityValuePaths[i].path),
                    dataComplexityValuePaths[i].complexityValuePath.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable6()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataNodeCoverageValues.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataIndependentPaths[i].id.ToString(),
                    PathToString(dataNodeCoverageValues[i].path),
                    dataNodeCoverageValues[i].nodeCoverage.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable7()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataDecisionNodeCoverageValue.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataIndependentPaths[i].id.ToString(),
                    PathToString(dataDecisionNodeCoverageValue[i].path),
                    dataDecisionNodeCoverageValue[i].decisionoNode.ToString()
                });
            }

            return data;
        }
        
        public List<string[]> DataTable8()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < DataWeightAssignedToEdges.Count; i++)
            {
                data.Add(new string[3]
                {
                    (DataWeightAssignedToEdges[i].from.ToString() + "-" + DataWeightAssignedToEdges[i].to.ToString()),
                    DataWeightAssignedToEdges[i].typeOfCoupling,
                    DataWeightAssignedToEdges[i].weight.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable9()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataWeightOfPathsBasedOnCoupling.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataIndependentPaths[i].id,
                    PathToString(dataWeightOfPathsBasedOnCoupling[i].path),
                    dataWeightOfPathsBasedOnCoupling[i].weightOfEachPath.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable10()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataTotalWeightOfEachPath.Count; i++)
            {
                data.Add(new string[3]
                {
                    dataIndependentPaths[i].id,
                    PathToString(dataTotalWeightOfEachPath[i].path),
                    dataTotalWeightOfEachPath[i].totalWeight.ToString()
                });
            }

            return data;
        }

        public List<string[]> DataTable11()
        {
            List<string[]> data = new List<string[]>();

            for (int i = 0; i < dataTotalWeightOfEachPath.Count; i++)
            {
                data.Add(new string[4]
                {
                    dataIndependentPaths[i].scenario,
                    dataIndependentPaths[i].id,
                    PathToString(dataTotalWeightOfEachPath[i].path),
                    dataTotalWeightOfEachPath[i].totalWeight.ToString()
                });
            }

            data.Sort(new ComparingDataTable11());

            return data;
        }
    }
    class ComparingDataTable11 : IComparer<string[]>
    {
        public int Compare(string[] x, string[] y)
        {
            double xVal = double.Parse(x[3]);
            double yVal = double.Parse(y[3]);

            if (xVal == 0 || yVal == 0)
                return 0;

            return yVal.CompareTo(xVal);
        }
    }

}

