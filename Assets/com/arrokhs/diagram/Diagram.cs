﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum ShapeType
{
    DEFAULT = 0,
    INITIAL_NODE = 1,
    ACTIVITY = 2,
    ACTIVITY_FINAL_NODE = 3,
    DECISION_NODE = 4,
    ACTIVITY_ACTION = 5,
    CONTROL_FLOW = 6
}

[Serializable]
public class Diagram
{
    [SerializeField]
    private string _id;
    [SerializeField]
    private int _value = 0;
    [SerializeField]
    private string _model;
    [SerializeField]
    private ShapeType _shapeType;
    [SerializeField]
    private float _x;
    [SerializeField]
    private float _y;
    [SerializeField]
    private float _width;
    [SerializeField]
    private float _height;

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public int Value
    {
        //get { return _shapeType == ShapeType.InitialNode ? 1 : _value; }
        get { return _value; }
        set { _value = value; }
    }

    public string Model
    {
        get { return _model; }
        set { _model = value; }
    }

    public ShapeType ShapeType
    {
        get { return _shapeType; }
        set { _shapeType = value; }
    }

    public float X
    {
        get { return _x * 0.01f; }
        set { _x = value; }
    }

    public float Y
    {
        get { return _y * 0.01f; }
        set { _y = value; }
    }

    public float Width
    {
        get { return _width; }
        set { _width = value; }
    }

    public float Height
    {
        get { return _height; }
        set { _height = value; }
    }

    public string SetShapeType(string shapeType)
    {
        switch (shapeType)
        {
            case "InitialNode":
                _shapeType = ShapeType.INITIAL_NODE;
                break;
            case "ActivityFinalNode":
                _shapeType = ShapeType.ACTIVITY_FINAL_NODE;
                break;
            case "Activity":
                _shapeType = ShapeType.ACTIVITY;
                break;
            case "DecisionNode":
                _shapeType = ShapeType.DECISION_NODE;
                break;
            case "ControlFlow":
                _shapeType = ShapeType.CONTROL_FLOW;
                break;
            default:
                _shapeType = ShapeType.DEFAULT;
                break;
        }
        return shapeType;
    }
}
