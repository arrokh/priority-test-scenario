﻿using com.arrokh;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

[System.Serializable]
public class Edge : Diagram
{
    private string _connectorStyle;
    private string _from;
    private string _to;

    public string ConnectorStyle
    {
        get { return _connectorStyle; }
        set { _connectorStyle = value; }
    }

    public string From
    {
        get { return _from; }
        set { _from = value; }
    }

    public string To
    {
        get { return _to; }
        set { _to = value; }
    }

    public Edge()
    {

    }

    public Edge(JToken data)
    {
        Id = (string)data["id"];
        Model = (string)data["model"];
        SetShapeType((string)data["shapeType"]);
        Width = float.Parse((string)data["width"], System.Globalization.CultureInfo.InvariantCulture);
        Height = float.Parse((string)data["height"], System.Globalization.CultureInfo.InvariantCulture);
        Id = (string)data["id"];
        Model = (string)data["model"];
        ConnectorStyle = (string)data["connectorStyle"];
        From = (string)data["from"];
        To = (string)data["to"];
    }

    public int From2Val(List<Node> shapes)
    {
        return shapes.Find(x => x.Id == _from).Value;
    }

    public int To2Val(List<Node> shapes)
    {
        return shapes.Find(x => x.Id == _to).Value;
    }
}
