﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;


namespace com.arrokh
{

    [System.Serializable]
    public class Node : Diagram
    {
        private List<Node> _in = new List<Node>();
        private List<Node> _out = new List<Node>();
        private string _text;

        public string Text
        {
            get { return _text; }
        }
        public int IDLabel
        {
            get { return int.Parse(_text.Split(' ')[0]); }
        }

        public List<Node> In
        {
            get { return _in; }
        }
        public List<int> InToIDLabel
        {
            get
            {
                List<int> listIDLabel = new List<int>();
                foreach (var node in _in)
                    listIDLabel.Add(node.IDLabel);
                return listIDLabel;
            }
        }

        public List<Node> Out
        {
            get { return _out; }
        }
        public List<int> OutInIDLabel
        {
            get
            {
                List<int> listIDLabel = new List<int>();
                foreach (var node in _out)
                    listIDLabel.Add(node.IDLabel);
                return listIDLabel;
            }
        }

        public Node(JToken data)
        {
            Id = (string)data["id"];
            Model = (string)data["model"];
            SetShapeType((string)data["shapeType"]);
            Width = float.Parse((string)data["width"], System.Globalization.CultureInfo.InvariantCulture);
            Height = float.Parse((string)data["height"], System.Globalization.CultureInfo.InvariantCulture);
            X = float.Parse((string)data["x"], System.Globalization.CultureInfo.InvariantCulture);
            Y = float.Parse((string)data["y"], System.Globalization.CultureInfo.InvariantCulture);
            _text = (string)data["name"];
        }

        public Node()
        {
        }

        private bool IsFirst()
        {
            return ShapeType == ShapeType.INITIAL_NODE ? true : false;
        }

        private bool IsFirst(List<Node> shapes)
        {
            foreach (var shape in shapes)
                return shape.IsFirst();
            return false;
        }

        private bool IsLast()
        {
            return ShapeType == ShapeType.ACTIVITY_FINAL_NODE ? true : false;
        }

        public static void RearrangeEdges(ref List<Node> nodes, List<Edge> edges)
        {
            if (nodes.Count == 0)
                return;

            foreach (var shape in nodes)
            {
                foreach (var connector in edges)
                {
                    if (connector.From == shape.Id)
                    {
                        foreach (var outShape in nodes)
                        {
                            if (connector.To == outShape.Id)
                                shape.Out.Add(outShape);
                        }
                    }
                    else if (connector.To == shape.Id)
                    {
                        foreach (var inShape in nodes)
                        {
                            if (connector.From == inShape.Id)
                                shape.In.Add(inShape);
                        }
                    }
                }
            }
        }
    }

}
