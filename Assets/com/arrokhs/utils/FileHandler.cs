﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SFB;
using System.IO;
using System.Xml;

namespace com.arrokhs.utils
{
    // Require
    // - StandaloneFileBrowser
    // - Newtonsoft
    public class EasyHandler
    {
        public static JObject XML2JObjectByOpen(ref string tempPath)
        {
            if (tempPath == null || tempPath == "")
            {
                var extensions = new[] { new ExtensionFilter("Select XML Files", "xml") };
                tempPath = @StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, true)[0];
            }

            return CreateFromXML(tempPath);
        }


        private static JObject CreateFromXML(string path)
        {
            StreamReader streamReader = File.OpenText(path);
            string xmlData = streamReader.ReadToEnd();
            streamReader.Close();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlData);

            string jsonText = JsonConvert.SerializeXmlNode(doc);

            //////////////////////////////////////
            // Clean unnecessary text XML exported
            // from Visual Paradigm
            //////////////////////////////////////
            jsonText = jsonText.Remove(153, 266);
            jsonText = jsonText.Remove(2, 46);
            jsonText = jsonText.Replace("\"@", "\"");
            //////////////////////////////////////

            return JObject.Parse(jsonText);
        }
    }
}
