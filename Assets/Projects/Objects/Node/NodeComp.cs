﻿using com.arrokh;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeComp : MonoBehaviour
{
    [SerializeField]
    private Node _shape;

    public Node Shape
    {
        get { return _shape; }
        set { _shape = value; }
    }

    public void UpdatePos()
    {
        Vector3 temp = new Vector3 {
            x = _shape.X,
            y = _shape.Y,
        };

        transform.localPosition = temp;
    }

    private void Start()
    {
        GetComponentInChildren<TextMeshPro>().text = (_shape.Value + 1).ToString();
    }
}
