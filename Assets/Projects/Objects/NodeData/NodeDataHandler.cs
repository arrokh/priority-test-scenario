﻿using System.Collections;
using System.Collections.Generic;
using com.arrokh;
using com.arrokhs.algorithms;
using TMPro;
using UnityEngine;
using static com.arrokhs.algorithms.TestScenarioPriotization;

public class NodeDataHandler : MonoBehaviour
{
    private TMP_Dropdown dropDown;

    [SerializeField]
    private int option = 0;

    [SerializeField]
    private int index = -1;

    public int IDLabel { get => index; }
    public int Option { get => option; }

    private void Start()
    {
        dropDown = GetComponentInChildren<TMP_Dropdown>();
        dropDown.onValueChanged.AddListener(OnOptionsChange);
    }

    public void UpdatePanelData(Node node)
    {

        transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = node.Text;
        index = node.IDLabel;
    }

    public void OnOptionsChange(int val)
    {
        option = val;
    }
}