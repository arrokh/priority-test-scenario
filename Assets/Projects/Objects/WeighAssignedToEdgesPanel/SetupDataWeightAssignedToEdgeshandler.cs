﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static com.arrokhs.algorithms.TestScenarioPriotization;

public class SetupDataWeightAssignedToEdgeshandler : MonoBehaviour
{
    [SerializeField]
    private GameObject panelDataPrefab;

    private Transform contentScrollView;

    private Main main;

    private void OnEnable()
    {
        main = FindObjectOfType<Main>();

        contentScrollView = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0);

        GameObject tempDelete = Instantiate(panelDataPrefab);

        if (contentScrollView.childCount > 0)
        {
            for (int i = 0; i < contentScrollView.childCount; i++)
                Destroy(contentScrollView.GetChild(i).gameObject);
        }

        foreach (var edge in main.core.DataWeightAssignedToEdges)
        {
            GameObject temp = Instantiate(panelDataPrefab);
            temp.transform.SetParent(contentScrollView);
            temp.GetComponent<PanelDataHandler>().UpdatePanelData(edge);
            temp.transform.localScale = Vector3.one;
            temp.name = edge.index + ". " + edge.from + "-" + edge.to;
        }

        Vector3 pos = contentScrollView.localPosition;
        pos.y = 0;
        contentScrollView.localPosition = pos;

    }

    public void OnSaveData()
    {
        main = FindObjectOfType<Main>();

        List<WeightAssignedToEdges> dataEdges = new List<WeightAssignedToEdges>();
        foreach (var panelDataHandler in contentScrollView.GetComponentsInChildren<PanelDataHandler>())
            dataEdges.Add(panelDataHandler.EdgeData);

        main.core.UpdateDataWeightAssignedToEdge(dataEdges);

        gameObject.SetActive(false);


        main.ShowResult();
    }

}
