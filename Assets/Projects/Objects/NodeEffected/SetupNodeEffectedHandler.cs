﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static com.arrokhs.algorithms.TestScenarioPriotization;

public class SetupNodeEffectedHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject nodeDataPrefab;

    private Transform contentScrollView;

    private Main main;

    private void OnEnable()
    {
        main = FindObjectOfType<Main>();

        contentScrollView = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0);

        GameObject tempDelete = Instantiate(nodeDataPrefab);

        if (contentScrollView.childCount > 0)
        {
            for (int i = 0; i < contentScrollView.childCount; i++)
                Destroy(contentScrollView.GetChild(i).gameObject);
        }

        foreach (var node in main.core.Nodes)
        {
            GameObject temp = Instantiate(nodeDataPrefab);
            temp.transform.SetParent(contentScrollView);
            temp.GetComponent<NodeDataHandler>().UpdatePanelData(node);
            temp.transform.localScale = Vector3.one;
            temp.name = node.Text;
        }

        Vector3 posContent = contentScrollView.localPosition;
        posContent.y = 0;
        contentScrollView.localPosition = posContent;

    }

    public void OnSaveData()
    {
        NodeDataHandler[] nodeDataHandlers = contentScrollView.GetComponentsInChildren<NodeDataHandler>();
        List<int> mainEffectedNode = new  List<int>();
        foreach (var nodeData in nodeDataHandlers)
        {
            if (nodeData.Option == 1)
                mainEffectedNode.Add(nodeData.IDLabel);
        }

        main = FindObjectOfType<Main>();
        main.core.UpdateNodeComplexity(mainEffectedNode.ToArray());
        main.InitWeightAssignedToEdges();
    }

}
