﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class SimpleInitTable : MonoBehaviour
{
    [SerializeField]
    private int numberTable = 1;

    private Main main;
    private TableHandler tableHandler;

    private void Awake()
    {
        
        main = FindObjectOfType<Main>();
        tableHandler = GetComponent<TableHandler>();

    }

    private void OnEnable()
    {
        DecisionShowTable(numberTable);
    }

    public void DecisionShowTable(int index)
    {
        switch (index)
        {
            case 2:
                PassingDataToTable(
                    main.core.DataTable2(),
                    "Table 2. Information complexity table",
                    new string[4]
                    {
                        "Node",
                        "IN",
                        "OUT",
                        "IN x OUT"
                    });
                break;
            case 3:
                PassingDataToTable(
                    main.core.DataTable3(),
                    "Table 3. Weight of paths based on information complexity",
                    new string[3]
                    {
                        "Independent paths",
                        "Path description",
                        "Weight of each path"
                    });
                break;
            case 4:
                PassingDataToTable(
                    main.core.DataTable4(),
                    "Table 4. Complexity value of nodes",
                    new string[3]
                    {
                        "Node",
                        "Amount of node contribution",
                        "Complexity value of each node"
                    });
                break;
            case 5:
                PassingDataToTable(
                    main.core.DataTable5(),
                    "Table 5. Path complexity based on node complexity value",
                    new string[3]
                    {
                        "Independent paths",
                        "Path description",
                        "Complexity value of each path"
                    });
                break;
            case 6:
                PassingDataToTable(
                    main.core.DataTable6(),
                    "Table 6. Node coverage values",
                    new string[3]
                    {
                        "Independent paths",
                        "Path description",
                        "Node coverage of each path"
                    });
                break;
            case 7:
                PassingDataToTable(
                    main.core.DataTable7(),
                    "Table 7. Decision node coverage values",
                    new string[3]
                    {
                        "Independent paths",
                        "Path description",
                        "Decision node coverage of each path"
                    });
                break;
            case 8:
                PassingDataToTable(
                    main.core.DataTable8(),
                    "Table 8. Weight assigned to edges",
                    new string[3]
                    {
                        "Edge",
                        "Type of coupling",
                        "Weight"
                    });
                break;
            case 9:
                PassingDataToTable(
                    main.core.DataTable9(),
                    "Table 9. Weight of paths based on coupling factor",
                    new string[3]
                    {
                        "Independent paths",
                        "Path description",
                        "Weight of each path"
                    });
                break;
            case 10:
                PassingDataToTable(
                    main.core.DataTable10(),
                    "Table 10. Total weight of each path",
                    new string[3]
                    {
                        "Independent paths",
                        "Path description",
                        "Total weight of each path"
                    });
                break;
            case 11:
                PassingDataToTable(
                    main.core.DataTable11(),
                    "Table 11. Prioritized scenario",
                    new string[4]
                    {
                        "Scenario",
                        "Paths",
                        "Path description",
                        "Total weight of each path"
                    });
                break;
            default:
                break;
        }
    }

    private void PassingDataToTable(List<string[]> list, string v1, string[] v2)
    {
        tableHandler.GenerateTable(list);
        tableHandler.SetCaptionContent(v1);
        tableHandler.SetHeadContent(v2);

    }
}
