﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TableHandler : MonoBehaviour
{
    [SerializeField]
    private Transform captionPanel;

    [SerializeField]
    private Transform headPanel;

    [SerializeField]
    private Transform contentPanel;

    [SerializeField]
    private GameObject dataContentPrefab;

    [SerializeField]
    private GameObject headContentPrefab;

    [SerializeField]
    private GameObject rowContentPrefab;

    [SerializeField]
    private bool showDumbData = false;

    private void Start()
    {
        if(showDumbData)
            AddDumbData();
    }

    private void AddDumbData()
    {
        List<string[]> dumbData = new List<string[]>();
        dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });
        //dumbData.Add(new string[3] { "P0", "1-2-3-4-5-6-7-8-9-10-11-12-19-20-21", "62.73" });
        //dumbData.Add(new string[3] { "P1", "1-2-3-4-17-18-19-20-21", "34.78" });
        //dumbData.Add(new string[3] { "P2", "1-2-3-4-5-6-15-16-19-20-21", "45.10" });
        //dumbData.Add(new string[3] { "P3", "1-2-3-4-5-6-7-8-9-13-14-19-20-21", "59.57" });

        GenerateTable(dumbData);
        SetHeadContent(new string[3] { "Independent paths", "Path description", "Total weight" });
    }

    public void GenerateTable(List<string[]> data)
    {
        for (int i = 0; i < contentPanel.childCount; i++)
        {
            var temp = contentPanel.GetChild(i).gameObject;
            Destroy(temp);
        }

        foreach (var row in data)
        {
            var rowGameObject = Instantiate(rowContentPrefab);
            rowGameObject.transform.SetParent(contentPanel);
            rowGameObject.transform.localScale = Vector3.one;
            for (int i = 0; i < row.Length; i++)
            {
                var dataGameObject = Instantiate(dataContentPrefab);
                dataGameObject.GetComponentInChildren<TextMeshProUGUI>().text = row[i];
                dataGameObject.transform.SetParent(rowGameObject.transform);
                dataGameObject.transform.localScale = Vector3.one;
            }
        }

        contentPanel.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, data.Count * 100.0f);
    }
    public void SetCaptionContent(string caption)
    {
        captionPanel.GetComponentInChildren<TextMeshProUGUI>().text = caption;
    }
    public void SetHeadContent(string[] content)
    {
        for (int i = 0; i < headPanel.childCount; i++)
        {
            var temp = headPanel.GetChild(i).gameObject;
            Destroy(temp);
        }

        for (int i = 0; i < content.Length; i++)
        {
            var headerData = Instantiate(headContentPrefab);
            headerData.GetComponentInChildren<TextMeshProUGUI>().text = content[i];
            headerData.transform.SetParent(headPanel);
            headerData.transform.localScale = Vector3.one;
        }
    }
}
