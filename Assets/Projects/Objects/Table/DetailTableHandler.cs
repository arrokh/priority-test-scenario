﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailTableHandler : MonoBehaviour
{
    [SerializeField]
    private int min = 2;

    [SerializeField]
    private int max = 11;

    [SerializeField]
    private int counter = 0;


    [SerializeField]
    private Transform leftButton;

    [SerializeField]
    private Transform rightButton;

    [SerializeField]
    private Transform closeButton;

    [SerializeField]
    private Transform tableObject;

    private SimpleInitTable initTable;

    private void Start()
    {
        initTable = GetComponentInChildren<SimpleInitTable>();

        counter = min;

        initTable.DecisionShowTable(counter);


        leftButton.GetComponent<Button>().onClick.AddListener(leftButtonHandler);
        rightButton.GetComponent<Button>().onClick.AddListener(rightButtonHandler);

        leftButton.gameObject.SetActive(false);
    }

    private void rightButtonHandler()
    {
        if (!leftButton.gameObject.activeInHierarchy)
            leftButton.gameObject.SetActive(true);

        ++counter;
        initTable.DecisionShowTable(counter);

        if (counter == max)
            rightButton.gameObject.SetActive(false);
    }

    private void leftButtonHandler()
    {
        if (!rightButton.gameObject.activeInHierarchy)
            rightButton.gameObject.SetActive(true);

        --counter;
        initTable.DecisionShowTable(counter);

        if (counter == min)
            leftButton.gameObject.SetActive(false);
    }
}
