﻿using System.Collections;
using System.Collections.Generic;
using com.arrokhs.algorithms;
using TMPro;
using UnityEngine;
using static com.arrokhs.algorithms.TestScenarioPriotization;

public class PanelDataHandler : MonoBehaviour
{
    private TMP_Dropdown dropDown;

    [SerializeField]
    private int index = -1;

    private WeightAssignedToEdges edgeData;

    public int Index { get => index;  }
    public WeightAssignedToEdges EdgeData { get => edgeData; }

    private void Start()
    {
        dropDown = GetComponentInChildren<TMP_Dropdown>();
        dropDown.onValueChanged.AddListener(OnOptionsChange);
    }

    public void UpdatePanelData(WeightAssignedToEdges edge)
    {
        transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = edge.fromText + "\n" + edge.toText;
        index = edge.index;

        edgeData = edge;
        edgeData.typeOfCoupling = SetTypeOfCoupling(edgeData.weight);
    }

    public void OnOptionsChange(int val)
    {
        edgeData.weight = val;
        edgeData.typeOfCoupling = SetTypeOfCoupling(edgeData.weight);
    }

    private string SetTypeOfCoupling(int weight)
    {
        switch (weight)
        {
            case 1:
                return "data";
            case 2:
                return "stamp";
            case 3:
                return "control";
            default:
                return "no";
        }
    }
}