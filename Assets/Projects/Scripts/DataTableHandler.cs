﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataTableHandler : MonoBehaviour
{
    private Main main;

    [SerializeField]
    private Transform mainTable;

    [SerializeField]
    private Transform panelDetailTables;

    private void Awake()
    {
        main = FindObjectOfType<Main>();
    }

    public void ResetButtonHandler()
    {
        SceneManager.LoadScene(0);
    }

    public void DetailButtonHandler()
    {
        panelDetailTables.gameObject.SetActive(true);
    }

    public void ChangeNodeEffectButtonHandler()
    {
        main.CalculateHandler();
        gameObject.SetActive(false);
    }
}
