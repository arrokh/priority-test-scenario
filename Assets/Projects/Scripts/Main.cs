﻿using UnityEngine;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using com.arrokhs.utils;
using com.arrokhs.algorithms;
using UnityEngine.UI;
using TMPro;
using System;
using com.arrokh;

public class Main : MonoBehaviour
{
    private string xmlData = "";
    private string path = "";

    private List<Node> nodes;
    private List<Edge> edges;

    private JObject rawXMLData;

    public TestScenarioPriotization core;

    [SerializeField]
    private Transform weightAssignedToEdgesPanel;

    [SerializeField]
    private Transform nodeEffected;

    [SerializeField]
    private Transform dataTable;

    [SerializeField]
    private Image spawnFlowGraph;


    private void Start()
    {
        /*
         * PHASE 1
         * */
        GameObject canvas = GameObject.Find("Canvas");
        canvas.transform.GetChild(0).gameObject.SetActive(false);
        canvas.transform.GetChild(1).gameObject.SetActive(true);
        /************************/
    }

    public void OpenFileHandler()
    {
        /*
         * PHASE 2
         * */
        path = null;
        rawXMLData = EasyHandler.XML2JObjectByOpen(ref path);

        GameObject mainPanel = GameObject.Find("MainPanel");

        mainPanel.transform.GetChild(3).GetComponent<Button>().interactable = true;
        mainPanel.transform.GetChild(4).gameObject.SetActive(true);
        mainPanel.transform.GetChild(4).GetComponentInChildren<Text>().text = path;

        var rawNodes = rawXMLData["Project"]["Diagrams"]["Diagram"]["Shapes"]["Shape"];
        var rawEdges = rawXMLData["Project"]["Diagrams"]["Diagram"]["Connectors"]["Connector"];

        nodes = new List<Node>();
        edges = new List<Edge>();

        // Initialize shapes and connectors
        foreach (var rawNode in rawNodes)
        {
            Node node = new Node(rawNode);
            if (node.ShapeType != ShapeType.DEFAULT)
                nodes.Add(node);
        }

        foreach (var rawEdge in rawEdges)
        {
            Edge edge = new Edge(rawEdge);
            edges.Add(edge);
        }

        Node.RearrangeEdges(ref nodes, edges);

        // Mengubah komponen Image.sprite object "Spawn Flow Graph"
        // Sesuai dengan nama file project xml yang di pilih sebelumnya
        string[] pathCust = path.Split('\\');
        string fileName = pathCust[pathCust.Length - 1].Split('.')[0];
        spawnFlowGraph.sprite = Resources.Load("Defined-Images/" + fileName, typeof(Sprite)) as Sprite;
        /************************/

    }

    public void ParsingHandler()
    {
        /*
         * PHASE 3
         * */
        core = new TestScenarioPriotization(nodes);
        GameObject.Find("MainPanel").SetActive(false);
        GameObject.Find("Canvas").transform.GetChild(0).gameObject.SetActive(true);
        GameObject.Find("ParsingPanel").GetComponentInChildren<TextMeshProUGUI>().text = core.DataTable1();
        /************************/
    }

    public void CalculateHandler()
    {
        /*
         * PHASE 4
         * */
        nodeEffected.gameObject.SetActive(true);
        weightAssignedToEdgesPanel.gameObject.SetActive(false);
        /************************/
    }

    public void InitWeightAssignedToEdges()
    {
        /*
         * PHASE 5
         * */
        nodeEffected.gameObject.SetActive(false);
        weightAssignedToEdgesPanel.gameObject.SetActive(true);
        /************************/
    }

    internal void ShowResult()
    {
        /*
         * PHASE 6
         * */
        dataTable.gameObject.SetActive(true);

        nodeEffected.gameObject.SetActive(false);
        weightAssignedToEdgesPanel.gameObject.SetActive(false);
        /************************/
    }
}
